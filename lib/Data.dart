import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'dart:convert';
import 'package:convert/convert.dart';

class Data extends StatefulWidget {
  BluetoothDevice device;

  Data(this.device) : super();

  @override
  State<StatefulWidget> createState() => _DataState(device);
}

class _DataState extends State<Data> {
  HexEncoder _hexEncoder;

  BluetoothCharacteristic tx;
  BluetoothCharacteristic txNotify;
  BluetoothDevice device;
  static const UUID_service = 'be940000-7333-be46-b7ae-689e71722bd5';
  static const UUID = 'be940001-7333-be46-b7ae-689e71722bd5';
  static const UUID3 = 'be940003-7333-be46-b7ae-689e71722bd5';
  static const dataSport = ([3, 2, 7, 0, 0, 38, -105]);
  static const dataSport2 = ([3, 9, 9, 0, 1, 0, 3, -127, -50]);
  static const List<int> dataSalud = ([3, 2, 7, 0, 2, 100, -73]);
  static const List<int> dataSalud2 = ([3, 9, 9, 0, 1, 3, 5, 20, -5]);
  static const List<int> stopMeasuring = ([3, 2, 7, 0, 0, 38, -105]);
  static const List<int> dataHistoric = ([5, 6, 7, 0, 1, 115, -128]);
  static const List<int> dataHistoric2 = ([
    5,
    -128,
    7,
    0,
    1,
    -46,
    122
  ]); // Coincide con todos los historic y es ultimo antes de mostrar el resultado
  static const List<int> dataHistoric3 = ([5, 6, 7, 0, 0, 82, -112]);
  static const List<int> dataSleepHistoric = ([5, 4, 7, 0, 1, 27, 109]);
  static const List<int> dataSportHistoric = ([5, 2, 7, 0, 1, -126, 74]);
  var steps = 0;
  var kcal = 0;
  dynamic km = 0;
  var systolic = 0;
  var diastolic = 0;
  var hr = 0;

  _DataState(this.device);

  getCharacteristic() async {
    print('------------------------getCharacteristic');
    List<BluetoothService> services = await device.discoverServices();
    services.forEach((service) async {
      if (service.uuid.toString() == UUID_service.toString()) {
        // do something with service
        var characteristics = service.characteristics;
        print('----------Caracteristicas:');
        for (BluetoothCharacteristic c in characteristics) {
          if (c.uuid.toString() == UUID.toString()) {
            print('******----------------ENCONTRADO UUID2--------------******');
            tx = c;
            tx.setNotifyValue(true);
          } else if (c.uuid.toString() == UUID3.toString()) {
            print('******--------------ENCONTRADO UUID3---------------******');

            txNotify = c;
            txNotify.setNotifyValue(true);

            //  openid(txNotify, txNotify.uuid);

          }
        }
      }
    });
  }

  void openid(BluetoothCharacteristic tx, Guid uuid) {}

  void getSport() async {
    await tx.write(dataSport);
    await tx.write(dataSport2);

    /*   txNotify.value.listen((value) {
      // Sport Data
      if (value[1] != 3) {
        Uint8List input = Uint8List.fromList(value);
        ByteData bd = input.buffer.asByteData();

        setState(() {
          steps = bd.getUint16(4, Endian.little);
          km = bd.getUint16(6, Endian.little) / 1000;
          kcal = bd.getUint16(8, Endian.little);
        });

        print(steps);
        print(km);
        print(kcal);
      }
    });*/
  }

  void getHealth() async {
    await tx.write(dataSalud);
    await tx.write(dataSalud2);

    /* txNotify.value.listen((value) {
      print(value);
      // Health Data
      if (value[1] == 3) {
        print(value[4]);
        print(value[5]);
        print(value[6]);

        setState(() {
          systolic = value[4];
          diastolic = value[5];
          hr = value[6];
        });
      }
    });*/
  }

  void stopNotification() async {
    await tx.write(stopMeasuring, withoutResponse: true).whenComplete(() {
      tx.setNotifyValue(false);
    }).catchError((e) {
      print(e);
    });
  }

  void getHistoricSleep() async {
    await tx.write(dataSleepHistoric);
    await tx.write(dataHistoric2);
    await txNotify.setNotifyValue(true);

    txNotify.value.listen((value) {
      print('----------INICIO OnNotify-----------');

      print(value);
      print(value.length);
      int sum = 8;
      int num = 946656000;

      int beginTime = 0;
      int endTime = 0;

      List<int> btime = [
        value[3 + sum],
        value[2 + sum],
        value[1 + sum],
        value[0 + sum]
      ];
      List<int> etime = [
        value[7 + sum],
        value[6 + sum],
        value[5 + sum],
        value[4 + sum]
      ];

      Uint16List input1 = Uint16List.fromList(btime);
      ByteData bd1 = input1.buffer.asByteData();
      beginTime = (bd1.getUint32(0) + num) * 1000;
      print(btime);
      print(beginTime);

      Uint16List input2 = Uint16List.fromList(etime);
      ByteData bd2 = input2.buffer.asByteData();
      endTime = (bd2.getUint32(0) + num) * 1000;
      print(etime);
      print(endTime);

      List<int> dsCounts = [0, 1, 37, 0];
      Uint16List input3 = Uint16List.fromList(dsCounts);
      ByteData bd3 = input3.buffer.asByteData();
      int dscount = bd3.getUint32(0);
      print(dsCounts);
      print(dscount);

    });
  }

  void getHistoricSport() async {
    await tx.write(dataSportHistoric);
    await tx.write(dataHistoric2);
    await txNotify.setNotifyValue(true);

    /*  txNotify.value.listen((value) {
      print(value);
    });*/
  }

  @override
  void initState() {
    // TODO: implement initState
    getCharacteristic();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Mediciones'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Icon(Icons.directions_run),
                  onPressed: getSport,
                ),
                RaisedButton(
                  child: Icon(Icons.accessibility_new),
                  onPressed: getHealth,
                ),
                RaisedButton(
                  child: Icon(Icons.stop),
                  onPressed: stopNotification,
                ),
              ],
            ),
            ButtonBar(
              alignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Icon(Icons.history),
                  onPressed: getHistoricSleep,
                ),
                RaisedButton(
                  child: Icon(Icons.history),
                  onPressed: getHistoricSport,
                )
              ],
            ),
            Text('Steps: $steps'),
            Text('Kcal: $kcal'),
            Text('Km: $km'),
            Text('HR: $hr'),
            Text('Systolic: $systolic'),
            Text('Diastolic: $diastolic'),
          ],
        ),
      ),
    );
  }
}
